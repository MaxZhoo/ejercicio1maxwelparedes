function informacionPersonal({info}) {
 //Atom (atom), desarrollado por -> GitHub Inc.
 //Visual Studio Code (vsc), desarrollado por ->  Microsoft
 //Sublime Text (st), desarrollado por -> Jhon Skinner
    switch (info.editor) {
        case "vsc":
            console.log("***vsc desarrollado por Microsoft***");
            break;
        case "atom":
            console.log("***atom desarrollado por GitHub INC.***");
            break;
        case "st":
            console.log("***st desarrollado por Jhon Skinner***");
            break;
                
        default:
            console.log("No hay preferencia referencia con el editor propuesto.");
            break;
    }

    return info;
}

function preferenciaColor({color}) {

    if(color==="black") return "green";
    else if(color==="green") return "white";
    else if(color==="yellow") return "blue";
    else return "orange"
}

module.exports = {informacionPersonal,preferenciaColor}