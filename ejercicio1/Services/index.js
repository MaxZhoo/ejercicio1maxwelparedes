const {informacionPersonal,preferenciaColor} = require("../Controllers")


function servicio({info,color}) {
    
    const datosPersonales = informacionPersonal({info});
    
    if(info.age>18){

        const  gustoDeColor = preferenciaColor({color});

        return {datosPersonales,gustoDeColor}

    }else{

        console.log("Error: preferencia de color no ha sido ejecutado!!!.");
        
    }
    
}

module.exports = {servicio}